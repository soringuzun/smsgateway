#!/bin/bash

#Check if device is ready
checkDevice() {
    status=$(ifconfig | grep '192.168.8' | wc -l )
        if [ $status -eq 1 ] 
        then
            echo "Device is running"
        else 
            echo "Not running. Start the device"
            exit 2
            fi          
} 

checkDevice; 

time=$(date +"%Y-%m-%d %T")
token=$(curl -s -X GET "http://192.168.8.1/api/webserver/token" | grep token | tr -d -c 0-9)

#Start login process
login() {
    loginStatus=$(curl -s -X GET "http://192.168.8.1/api/user/state-login" | grep -i "admin" | wc -l )
    if [ $loginStatus -eq 1 ]
        then 
            echo "Ok. Already Logged in" 
        else
            curl -s -X POST "http://192.168.8.1/api/user/login" -H "Content-Type: application/xml" -H "X-Requested-With: XMLHttpRequest" -H "__RequestVerificationToken: $token" -d @login.xml > login.tmp
            loginStatus=$(cat login.tmp | grep -i "OK" | wc -l )
       
        if [ $loginStatus -eq 1 ]; then
            rm login.tmp
            echo "OK: Successfully logged in"
        else
            echo "ERROR: Failed to log in. Open Dashboard for errors."
            rm login.tmp
            exit 2
        fi 
    fi    
}

login

sleep 0.5

#Send message

sendMessage() {
    read -p "Enter Phone number: " phone
    read -p "Enter Message: " message 
    messageLenght=`echo ${#message}` 

#Create a temporary xml file with needed parameters
    xmlstarlet edit \
    --update "//request/Phones/Phone" \
    --value "$phone" \
    --update "//request/Content" \
    --value "$message" \
    --update "//request/Date" \
    --value "$time" \
    --update "//request/Length" \
    --value "$messageLenght" sample.xml > tmp.xml 
   
    curl -s -X POST "http://192.168.8.1/api/sms/send-sms"  -H "Content-Type: application/xml" -H "X-Requested-With: XMLHttpRequest" -H "__RequestVerificationToken: $token" -d @tmp.xml > tmp.status
    
    sendStatus=$(cat tmp.status | grep -i "OK" | wc -l )

    if [ $sendStatus -eq 1 ]
        then
            echo "SMS sended successfully"
        else
            echo "SMS not delivered. Try again" 
    fi   

    #Delete temporary file
    rm tmp.xml
    rm tmp.status
}

sendMessage;

exit;
